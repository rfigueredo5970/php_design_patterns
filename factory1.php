<?php

	class Database {

		private static $instance;

		private function __construct(){

		}


		public static function getInstance(){

			if(!isset(Database::$instance)){
				Database::$instance = new Database();
			}


			return Database::$instance;

		}

		public function getQuery(){
			echo "SELECT * FROM database";
		}
	}

	$db1 = Database::getInstance();

	$db1->getQuery();

	$db2 = Database::getInstance();
	var_dump($db2);

	$db3 = Database::getInstance();
	var_dump($db3);

